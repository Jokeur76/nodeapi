import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TestComponent} from "./test-component/test.component";
import {TestService} from "./test.service";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TestComponent],
  exports: [TestComponent],
  providers: [TestService]
})
export class TestModule { }
