import { Injectable }     from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { environment } from '../../../../environments/environment';
import { Test } from "./test.model";

@Injectable()
export class TestService {

  private baseUrl =  environment.serverUrl;
  constructor(
    private http: HttpClient
  ) { }

  getTestById(id) : Observable<Test> {
    // ...using get request
    return this.http.get(this.baseUrl + '/test/' + id)
    // ...now we return data
      .map((res: Test) => {
        return res;
      } )
      // ...errors if any
      .catch((error:any) => Observable.throw(error.message || 'Server error'));
  }
}
