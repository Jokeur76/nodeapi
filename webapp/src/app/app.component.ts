import { Component, OnInit } from '@angular/core';
import { TestService } from './objects/test/test.service';
import {Test} from "./objects/test/test.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private title;

  constructor(
    private testService: TestService
  ) {}

  ngOnInit(){
    // Load comments
    this.title = 'app';
    this.testService.getTestById('5ab34be8d8199941d6a12e2b')
      .subscribe((test: Test) => {
        this.title = test.message;

      })
  }

}
