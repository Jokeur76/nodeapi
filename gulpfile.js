const gulp = require('gulp');
const ts = require('gulp-typescript');
const JSON_FILES = ['server/src/*.json', 'server/src/**/*.json'];

const dest = 'server/lib';
const src = 'server/src/**/*.ts';

// pull in the project TypeScript config
const tsProject = ts.createProject('tsconfig.json');

gulp.task('scripts', () => {
    const tsResult = tsProject.src()
        .pipe(tsProject());
    return tsResult.js.pipe(gulp.dest(dest));
});

gulp.task('watch', ['scripts'], () => {
    gulp.watch(src, ['scripts']);
});

gulp.task('assets', function() {
    return gulp.src(JSON_FILES)
        .pipe(gulp.dest(dest));
});

gulp.task('default', ['watch', 'assets']);