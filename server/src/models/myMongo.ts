import { MongoClient, ObjectId } from 'mongodb';
import * as assert from "assert";
import { environment } from "../environments/environment";

export class MyMongo {
    protected db;
    private url = 'mongodb://localhost:27017/';
    public connected = false;
    protected collection;

    constructor(collection = '') {
        this.url = this.url + environment.dbName;
        this.collection = collection;
        this.connect();
    }

    public get(req, res, next, search?) {
        if (this.connected) {
            this.db.collection(this.collection).find(search ? search : req.body.search).toArray((error, results) => {
                assert.equal(error, null);
                res.json(results);
            })
        } else {
            res.json();
        }
    }

    public getById(req, res, next) {
        if (this.connected) {
            this.db.collection(this.collection).find({_id: new ObjectId(req.params.id)}).toArray((error, results) => {
                assert.equal(error, null);
                if (results.length === 1) {
                    res.json(results[0]);
                } else if (results.length === 0) {
                    res.json();
                } else {
                    throw "too much results";
                }
            })
        } else {
            res.json();
        }
    }

    public update() {

    }

    public create() {

    }

    public remove() {

    }

    public connect() {
        let that = this;
        MongoClient.connect(this.url, function(err, client){
            assert.equal(null, err);
            that.db = client.db(environment.dbName);
            that.connected = true;
        });
    }

}