import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';

import { Test } from "./models/test";

// Creates and configures an ExpressJS web server.
class App {

    // ref to Express instance
    public express: express.Application;
    private test: Test = new Test();

    //Run configuration methods on the Express instance.
    constructor() {
        this.express = express();
        this.middleware();
        this.routes();
    }

    // Configure Express middleware.
    private middleware(): void {
        this.express.use(logger('dev'));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
        this.express.use(function(req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Origin");
            res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
            res.header('Access-Control-Expose-Headers', 'Authorization');
            // res.header("Allow", "OPTIONS, GET, POST, PUT, DELETE, HEAD");
            next();
        });
    }

    // Configure API endpoints.
    private routes(): void {
        /* This is just to get up and running, and to make sure what we've got is
         * working so far. This function will change when we start to add more
         * API endpoints */
        let router = express.Router();
        // placeholder route handler
        router.get('/test', (req, res, next) => this.test.get(req, res, next));
        router.get('/test/:id', (req, res, next) => this.test.getById(req, res, next));
        this.express.use('/', router);

    }

}

export default new App().express;